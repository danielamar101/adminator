This script is intended to run on all company machines at Olive in order to create an Administrator account with Secure Token Access. 

GUI and Password validation implemented from:
github.com/homebysix/jss-filevault-reissue/

More information on Secure Token Access and sysadminctl here: https://krypted.com/mac-os-x/using-sysadminctl-macos/