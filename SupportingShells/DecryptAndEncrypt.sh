#!/bin/bash

#  DecryptAndEncrypt.sh
#  
#
#  Author: Daniel Amar
#  Description: Functions that are meant for local use on a machine to generate password, salt and passphrase for use with adminator script
#  Obtained From: https://github.com/jamf/Encrypted-Script-Parameters
#  

function GenerateEncryptedString() {
    # Usage ~$ GenerateEncryptedString "String"
    local STRING="${1}"
    local SALT=$(openssl rand -hex 8)
    local K=$(openssl rand -hex 12)
    local ENCRYPTED=$(echo "${STRING}" | openssl enc -aes256 -a -A -S "${SALT}" -k "${K}")
    echo "Encrypted String: ${ENCRYPTED}"
    echo "Salt: ${SALT} | Passphrase: ${K}"
}

#Enter whatever you want into the first paramameter of GenerateEncryptedString
GenerateEncryptedString 'ENTER PASSWORD TO ENCRYPT HERE'

function DecryptString() {
    # Usage: ~$ DecryptString "Encrypted String" "Salt" "Passphrase"
    echo "${1}" | /usr/bin/openssl enc -aes256 -d -a -A -S "${2}" -k "${3}"
}
echo 'Decrypting: PASSWORD'

#First parameter is where the encrypted password goes.
DecryptString 'PUT PASSWORD HERE' 'ebd03ecf39824d03' 'b6cb84567686b751169a97d4'
